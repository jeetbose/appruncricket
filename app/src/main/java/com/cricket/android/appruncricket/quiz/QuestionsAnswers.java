package com.cricket.android.appruncricket.quiz;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class QuestionsAnswers {

    private List<String> questions;
    List<List<String>> options;

    public QuestionsAnswers() { }

    public QuestionsAnswers(List<String> questions, List<List<String>> options) {
        this.questions = questions;
        this.options = options;
    }

    public List<String> getQuestions() {
        return questions;
    }


    /**
     * function to load the questions from csv file or at this time it is hard coded
     */
    public void setQuestions() {
        this.questions =  Arrays.asList(
                "When and where was the first official international cricket match played ?",
                "Who scored the highest number of centuries ?",
                "Who holds the record for taking the highest number of wickets ?",
                "Which cricketers have the record for the highest run partnership at the World Cup ?",
                "Which teams were involved in the highest scoring match of all times in One Day Internationals ?",
                "Which two South African batsmen were involved in the famous run-out incident in the 1999 World Cup semi-final against Australia ?",
                "What was Matthew Hayden’s final score after breaking Brian Lara’s world record? ?",
                "Who captained Sri Lanka to victory at the World Cup final in 1996 ?",
                "Which team did Sachin Tendulkar score his 50th test hundred against? ?");
    }

    /**
     *
     * @return the list of answer options for each questions
     */

    public List<List<String>> getAnswers() {
        return options;
    }

    /**
     *
     * sets the answers for the questions
     */
    public void setAnswers() {

        options = new ArrayList<>(this.questions.size());

        options.add( Arrays.asList("1877", "1912", "1844", "1975"));
        options.add( Arrays.asList("Steve Waugh", "Sachin Tendulkar", "Donald Bradman", "Brian Lara"));
        options.add( Arrays.asList("Anil Kumble", "Shane Warne", "Muttiah Muralitharan", "Glenn Mcgrath"));

        options.add( Arrays.asList("Sachin Tendulkar", "Virender Sehwag", "Rahul Dravid", "Saurav Ganguly"));
        options.add( Arrays.asList("Australia", "India", "South Africa", "England"));
        options.add( Arrays.asList("Allan Donald", "Lance Klusener", "Jonty Rhodes", "Sean Pollock"));

        options.add( Arrays.asList( "385", "380"));
        options.add( Arrays.asList(" Aravinda de Silva", "Arjuna Ranatunga", "Sanath Jayasuriya"));
        options.add( Arrays.asList("Australia", "South Africa"));

    }


    /**
     *
     * @param answers
     */
    public Map<Integer, List<String>> loadAnswers(Map<Integer, List<String>> answers) {

        answers.put(0, Arrays.asList("1877"));  answers.put(1, Arrays.asList("Sachin Tendulkar"));
        answers.put(2, Arrays.asList("Muttiah Muralitharan"));

        answers.put(3, Arrays.asList("Sachin Tendulkar", "Saurav Ganguly"));  answers.put(4, Arrays.asList("Australia", "South Africa"));
        answers.put(5, Arrays.asList("Allan Donald", "Lance Klusener"));  answers.put(6, Arrays.asList("380"));
        answers.put(7, Arrays.asList("Arjuna Ranatunga"));  answers.put(8, Arrays.asList("South Africa"));

        return answers;

    }
}
