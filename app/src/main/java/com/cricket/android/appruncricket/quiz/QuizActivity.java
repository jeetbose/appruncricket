package com.cricket.android.appruncricket.quiz;

// Photo by Diego PH on Unsplash
// Photo by César Couto on Unsplash
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.cricket.android.appruncricket.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class QuizActivity extends AppCompatActivity {

    TextView questionNumber, quizTimer, quizQuestion, score_text;
    TextView quizOption1, quizOption2, quizOption3, quizOption4;
    CheckBox quizCheckOption1, quizCheckOption2, quizCheckOption3, quizCheckOption4;
    RadioButton quizRadioOption1, quizRadioOption2, quizRadioOption3, quizRadioOption4;


    View quiz_bottomView, submitLayout, quiz_topView;
    RadioGroup quiz_option_radio_group;
    Button  reset_button, submit_button;
    CountDownTimer cTimer = null;

    List<String> questions;
    List<List<String>> options;
    Map<Integer, List<String>> answers;

    private final int QUESTIONS_FIRST_LIST = 3;
    private final int QUESTIONS_SECOND_LIST = 6;
    private final int QUESTIONS_THIRD_LIST = 9;

    int current_question_number = 0;
    int score = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        initViews();
    }


    public void initViews() {

        quiz_bottomView =  findViewById(R.id.quiz_bottomView);
        submitLayout = findViewById(R.id.submitLayout);
        quiz_topView = findViewById(R.id.quiz_topView);

        questionNumber = findViewById(R.id.quiz_question_num);
        quizTimer = findViewById(R.id.quiz_timer);
        quizQuestion = findViewById(R.id.quiz_question);
        score_text = findViewById(R.id.score_text);


        quiz_bottomView.setVisibility(View.VISIBLE);
        submitLayout.setVisibility(View.GONE);
        score_text.setVisibility(View.GONE);

        quizOption1 = findViewById(R.id.quiz_option1);
        quizOption2 = findViewById(R.id.quiz_option2);
        quizOption3 = findViewById(R.id.quiz_option3);
        quizOption4 = findViewById(R.id.quiz_option4);

        quizCheckOption1 = findViewById(R.id.quiz_option_checkbox1);
        quizCheckOption2 = findViewById(R.id.quiz_option_checkbox2);
        quizCheckOption3 = findViewById(R.id.quiz_option_checkbox3);
        quizCheckOption4 = findViewById(R.id.quiz_option_checkbox4);

        quizRadioOption1 = findViewById(R.id.quiz_option_radio1);
        quizRadioOption2 = findViewById(R.id.quiz_option_radio2);
        quiz_option_radio_group = findViewById(R.id.quiz_question_radio_group1);

        reset_button = findViewById(R.id.reset_button);

        submit_button = findViewById(R.id.submit_button);

        reset_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                quiz_bottomView.setVisibility(View.VISIBLE);
                quiz_topView.setVisibility(View.VISIBLE);
                reset_button.setVisibility(View.GONE);
                submit_button.setVisibility(View.GONE);
                questionLoad(0);
                resetCheckBoxes();
                resetRadioButtons();
            }
        });


        QuestionsAnswers qa = new QuestionsAnswers();
        qa.setQuestions();
        qa.setAnswers();

        questions = qa.getQuestions();
        options = qa.getAnswers();
        answers = new HashMap<Integer, List<String>>();

        for(int i = 0; i < questions.size(); i++) {
            answers.put(new Integer(i), new ArrayList<String>());
        }

        questionLoad(0);
        answers = qa.loadAnswers(answers);

    }


    /**
     * reset to initial question view
     */
    public void questionLoad(int question_number) {


        questionNumber.setText(String.valueOf(current_question_number+1) + "/" + questions.size());

        quizQuestion.setText(questions.get(question_number));

        if(question_number < QUESTIONS_FIRST_LIST) {
            quizOption1.setText(options.get(question_number).get(0));
            quizOption2.setText(options.get(question_number).get(1));
            quizOption3.setText(options.get(question_number).get(2));
            quizOption4.setText(options.get(question_number).get(3));

        } else if(question_number < QUESTIONS_SECOND_LIST) {

            resetCheckBoxes();
            quizCheckOption1.setText(options.get(question_number).get(0));
            quizCheckOption2.setText(options.get(question_number).get(1));
            quizCheckOption3.setText(options.get(question_number).get(2));
            quizCheckOption4.setText(options.get(question_number).get(3));


        }
        else if(question_number < QUESTIONS_THIRD_LIST) {

            resetRadioButtons();
            quizRadioOption1.setText(options.get(question_number).get(0));
            quizRadioOption2.setText(options.get(question_number).get(1));

        }

        startTimer();
    }

    /**
     * validate question size
     */
    private void validateQuestion(){

        cancelTimer();

        View questionLayout = findViewById(R.id.quiz_option_menu);
        View checkQuestionLayout = findViewById(R.id.quiz_option_menu_checkbox);
        View radioQuestionLayout = findViewById(R.id.quiz_option_menu_radio);

        if(current_question_number < QUESTIONS_FIRST_LIST-1)
            questionLoad(++current_question_number);
        else if(current_question_number < QUESTIONS_SECOND_LIST-1){

            questionLayout.setVisibility(View.GONE);
            checkQuestionLayout.setVisibility(View.VISIBLE);
            radioQuestionLayout.setVisibility(View.GONE);

            questionLoad(++current_question_number);

        }
        else if(current_question_number < QUESTIONS_THIRD_LIST-1){

            questionLayout.setVisibility(View.GONE);
            checkQuestionLayout.setVisibility(View.GONE);
            radioQuestionLayout.setVisibility(View.VISIBLE);
            questionLoad(++current_question_number);

        }else {
            quiz_topView.setVisibility(View.GONE);
            quiz_bottomView.setVisibility(View.GONE);
            submitLayout.setVisibility(View.VISIBLE);


        }
    }

    // start timer
    void startTimer() {
        cTimer = new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                quizTimer.setText(String.valueOf(millisUntilFinished/1000) + " secs");
            }
            public void onFinish() {
                if(cTimer != null) {
                    cTimer.cancel();
                    validateQuestion();
                }
            }
        };
        cTimer.start();
    }

    //cancel timer
    void cancelTimer() {
        if(cTimer!=null)
            cTimer.cancel();
    }

    /**
     * Method triggered when user clicks on Options
     * @param view
     *
     */

    public void clickOptionBox(View view) {

        Log.v("QuizActivity", ((TextView)view).getText().toString());
        String answer = ((TextView)view).getText().toString();

        if(answer.equalsIgnoreCase(answers.get(current_question_number).get(0))){
            score++;
        }
        validateQuestion();

    }


    public void clickRadioBox(View view) {

        switch(view.getId()) {
            case R.id.quiz_option_radio1:
                quizRadioOption2.setChecked(false);
                break;
            case R.id.quiz_option_radio2:
                quizRadioOption1.setChecked(false);
                break;
        }
        Log.v("QuizActivity", view.toString());
    }


    /**
     *
     * @param view
     */
    public void nextCheckBoxQuestion(View view) {

       List<String> actual_answers_check = answers.get(current_question_number);
       List<String> user_answers = new ArrayList<>();

        if(quizCheckOption1.isChecked()) user_answers.add(quizCheckOption1.getText().toString());
        if(quizCheckOption2.isChecked()) user_answers.add(quizCheckOption2.getText().toString());
        if(quizCheckOption3.isChecked()) user_answers.add(quizCheckOption3.getText().toString());
        if(quizCheckOption4.isChecked()) user_answers.add(quizCheckOption4.getText().toString());


        Collections.sort(user_answers);
        Collections.sort(actual_answers_check);

        if(user_answers.equals(actual_answers_check))
            score++;

        validateQuestion();
    }

    /**
     *
     * @param view
     */
    public void nexRadioQuestion(View view) {

        String actual_answers_check = answers.get(current_question_number).get(0);
        String user_answers = "";

        if(quizRadioOption1.isChecked())
            user_answers = quizRadioOption1.getText().toString();
        else if (quizRadioOption2.isChecked())
            user_answers = quizRadioOption2.getText().toString();


        if(user_answers.equals(actual_answers_check))
            score++;

        quiz_option_radio_group.clearCheck();
        validateQuestion();
    }

    public void submitAnswers(View view) {

        if(score > 6) {
            Toast.makeText(this, "Excellent job, scored " + score + " out of " + questions.size(), Toast.LENGTH_SHORT).show();

        } else if(score > 4 && score <= 6) {
            Toast.makeText(this, "Decent job, scored " + score + " out of " + questions.size(), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Poor job, scored " + score + " out of " + questions.size(), Toast.LENGTH_SHORT).show();
        }

        submit_button.setVisibility(View.GONE);
        reset_button.setVisibility(View.GONE);
        quiz_topView.setVisibility(View.GONE);

        score_text.setText("" + score + "/" + questions.size());
        score_text.setVisibility(View.VISIBLE);
    }

    public void resetCheckBoxes() {
        quizCheckOption1.setChecked(false);
        quizCheckOption2.setChecked(false);
        quizCheckOption3.setChecked(false);
        quizCheckOption4.setChecked(false);
    }

    public void resetRadioButtons() {
        quizRadioOption1.setChecked(false);
        quizRadioOption2.setChecked(false);

    }
}
