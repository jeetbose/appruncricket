package com.cricket.android.appruncricket.bookcricket;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.cricket.android.appruncricket.R;
import com.cricket.android.appruncricket.bookcricket.Score;

public class ScoreAdapter extends ArrayAdapter<Score> {

    Score[] scores;
    public ScoreAdapter(Context context, Score[] objects) {
        super(context, 0, objects);
        this.scores = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Score batsman = scores[position];

        if(convertView == null) {
            final LayoutInflater layoutInflator = LayoutInflater.from(getContext());
            convertView = layoutInflator.inflate(R.layout.list_batsman,parent, false);
        }

        TextView name = (TextView) convertView.findViewById(R.id.batsman_text_view);
        TextView score = (TextView) convertView.findViewById(R.id.batsman_score_text_view);

        name.setText(batsman.getName());
        score.setText(batsman.getScore());

        return convertView;
    }
}
