package com.cricket.android.appruncricket;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.cricket.android.appruncricket.bookcricket.PlayCricketActivity;
import com.cricket.android.appruncricket.quiz.QuizActivity;


public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
    }

    public void initViews() {

        TextView quizApp = findViewById(R.id.quiz_menu);
        TextView cricketApp = findViewById(R.id.cricket_menu);


        quizApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent quizIntent = new Intent(getApplicationContext(), QuizActivity.class);
                startActivity(quizIntent);
            }
        });

        cricketApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cricketIntent = new Intent(getApplicationContext(), PlayCricketActivity.class);
                startActivity(cricketIntent);
            }
        });
    }

}
