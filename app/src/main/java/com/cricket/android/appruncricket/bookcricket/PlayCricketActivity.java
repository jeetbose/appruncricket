package com.cricket.android.appruncricket.bookcricket;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.cricket.android.appruncricket.R;

import java.util.Arrays;
import java.util.Random;

public class PlayCricketActivity extends AppCompatActivity {

    final int MAX_OUT = 6;
    final int MAX_THROWS_OVER = 6;
    final int MAX_OVERS = 1;

    int scoreTeamA = 0; int num_balls_A = 0;
    int scoreTeamB = 0; int num_balls_B = 0;
    int outTeamA = 0; int oversTeamA = 0;
    int outTeamB = 0; int oversTeamB = 0;
    int[] batsmen_A = new int[MAX_OUT];
    int[] batsmen_B = new int[MAX_OUT];

    int batting_team_count = 0;

    Score[] batsmenA;
    Score[] batsmenB;
    Button teamAButton, teamBButton;

    TextView team_run_a; TextView last_ball_run_a; TextView overs_bowled_a;
    TextView team_run_b; TextView last_ball_run_b; TextView overs_bowled_b;
    TextView current_batting_team;

    LinearLayout layoutTeamA;
    LinearLayout layoutTeamB;

    // -1 signifies out, 0 for 0 run, ...
    /**
     *  -1 signifies out, 0 for 0 run, ...
     *
     */
    final int[] run_array = {-1, 0, 1, 2, 4, 6};

    ScoreAdapter itemsAdapterA, itemsAdapterB;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_cricket);

        initBatsmen();
        initViews();

    }

    /**
     *
     * based on the toss result,  team batting first is decided
     */
    private void toggleButtons() {

        int toss_teamA = toss();

        if (toss_teamA == 1){
            // disable Team B controls
            teamBButton.setEnabled(false);
            teamAButton.setEnabled(true);
            layoutTeamA.setBackgroundColor(getResources().getColor(R.color.colorLayout));
            layoutTeamB.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));

            current_batting_team.setText("Batting Team - " + getResources().getText(R.string.team_a));

        } else {
            // disable Team A controls
            teamAButton.setEnabled(false);
            teamBButton.setEnabled(true);
            layoutTeamA.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
            layoutTeamB.setBackgroundColor(getResources().getColor(R.color.colorLayout));

            current_batting_team.setText("Batting Team - " + getResources().getText(R.string.team_b));
        }
    }

    /**
     *
     * Initialize the views
     */

    private void initViews() {

        team_run_a = (TextView) findViewById(R.id.team_a_score);
        last_ball_run_a = (TextView) findViewById(R.id.run_scored_a);
        overs_bowled_a = (TextView) findViewById(R.id.overs_a);

        team_run_b = (TextView) findViewById(R.id.team_b_score);
        last_ball_run_b = (TextView) findViewById(R.id.run_scored_b);
        overs_bowled_b = (TextView) findViewById(R.id.overs_b);

        team_run_a.setText("0/0"); team_run_b.setText("0/0");
        overs_bowled_a.setText("0.0"); overs_bowled_b.setText("0.0");
        last_ball_run_a.setText("0 runs"); last_ball_run_b.setText("0 runs");

        layoutTeamA = (LinearLayout) findViewById(R.id.team1_layout);
        layoutTeamB = (LinearLayout) findViewById(R.id.team2_layout);

        current_batting_team = (TextView) findViewById(R.id.batting_team_name);

        toggleButtons();

    }

    /**
     * initialize the batsmen name for the 2 teams
     * initialize the adapter for the batsmen list
     * assign initial bastmen scores to 0
     */

    private void initBatsmen() {

        batsmenA = (Score[]) Arrays.asList(new Score("Batsman1", ""), new Score("Batsman2", ""),
                new Score("Batsman3", ""), new Score("Batsman4", ""),
                new Score("Batsman5", ""), new Score("Batsman6", "")).toArray();
        batsmenB = (Score[])Arrays.asList(new Score("Batsman1", ""), new Score("Batsman2", ""),
                new Score("Batsman3", ""), new Score("Batsman4", ""),
                new Score("Batsman5", ""), new Score("Batsman6", "")).toArray();
        itemsAdapterA = new ScoreAdapter(this, batsmenA);
        itemsAdapterB = new ScoreAdapter(this, batsmenB);

        ListView listAView = (ListView) findViewById(R.id.list_batsmen_a);
        ListView listBView = (ListView) findViewById(R.id.list_batsmen_b);

        listAView.setAdapter(itemsAdapterA);
        listBView.setAdapter(itemsAdapterB);

        Arrays.fill(batsmen_A,0);
        Arrays.fill(batsmen_B,0);

        teamAButton = (Button) findViewById(R.id.run_button_a);
        teamBButton = (Button) findViewById(R.id.run_button_b);

    }

    /**
     * random number generator for the toss, if > 5, result throws 1 else 0
     * @return team 1 won the toss or not if 1 team1 won the toss else team 2
     */
    public int toss(){
        return Math.random()* 10 > 5 ? 1 : 0;
    }


    private boolean validateMaxOversReached(int overs) {

        return overs == MAX_OVERS ? true : false;
    }

    /**
     * cumulative score of team A
     * @param view
     */
    public void scoreTeamA(View view) {

        int run_index = new Random().nextInt(MAX_THROWS_OVER);
        if( (num_balls_A+1) % MAX_THROWS_OVER == 0 ) {
            oversTeamA++;
            num_balls_A = 0;
        } else { num_balls_A++; }

        if(run_array[run_index] == -1) {
            outTeamA++;
            batsmen_A[outTeamA] += 0;
            if(outTeamA == MAX_OUT){
                teamAButton.setEnabled(false);
            }

        } else {
            scoreTeamA += run_array[run_index];
            batsmen_A[outTeamA] += run_array[run_index];
        }

        if(oversTeamA == MAX_OVERS) {  //
            batting_team_count++;
            teamAButton.setEnabled(false);
        }

        displayToastAndToggleButton(oversTeamA, outTeamA, teamAButton, getResources().getText(R.string.team_a).toString());

        if(outTeamA <= MAX_OUT) {
            displayScore(run_index,"Team A");
        }

    }

    public void toggleBackGround(String team) {

        if(team.equalsIgnoreCase("Team A")) {
            layoutTeamB.setBackgroundColor(getResources().getColor(R.color.colorLayout));
            layoutTeamA.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));

            current_batting_team.setText("Batting Team - " + getResources().getText(R.string.team_b));

        } else {
            layoutTeamA.setBackgroundColor(getResources().getColor(R.color.colorLayout));
            layoutTeamB.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));

            current_batting_team.setText("Batting Team - " + getResources().getText(R.string.team_a));

        }

    }

    /**
     *
     * @param overs over consiss of 6 throws/balls, shows how many overs were bowled
     * @param outTeam number of  outs/ strikes for the batting team
     * @param button1 score button reference to enable/disable
     * @param team team which is batting
     */

    public void displayToastAndToggleButton(int overs, int outTeam, Button button1,  String team){


        Toast toast;

        if(validateMaxOversReached(overs) && outTeam == MAX_OUT) {
            button1.setEnabled(false);
            toast =  Toast.makeText(this,"Overs Completed and Bowled out!!! Team reached the score of + " + team + " for " + outTeam + " wickets", Toast.LENGTH_SHORT);
            toast.show();
            toggleBackGround(team);
        } else if(validateMaxOversReached(overs)) {

            button1.setEnabled(false);
            toast = Toast.makeText(this,"Overs Completed !!! Team reached the score of + " + team + " for " + outTeam + " wickets", Toast.LENGTH_SHORT);
            toast.show();
            toggleBackGround(team);
        } else if(outTeam == MAX_OUT) {

            button1.setEnabled(false);
            toast = Toast.makeText(this,"Bowled out!!! Team reached the score of + " + team + " for " + outTeam + " wickets", Toast.LENGTH_SHORT);
            toast.show();
            toggleBackGround(team);
        }

    }

    /**
     *
     * @param run_index  index of the run_array to show how many runs were scored on that throw/ball
     * @param team team name, the team that is batting
     */
    private void displayScore(int run_index, String team) {


        if(team == "Team A"){
            if( run_index == 0) {
                last_ball_run_a.setText("W");
            } else {
                last_ball_run_a.setText(""+ run_array[run_index]+" runs");
            }

            team_run_a.setText("" + scoreTeamA + "/" + outTeamA);

            batsmenA[outTeamA].setScore(String.valueOf(batsmen_A[outTeamA]));
            overs_bowled_a.setText("" + oversTeamA + "." + num_balls_A);
            itemsAdapterA.notifyDataSetChanged();

            teamBButton.setEnabled(true);


        } else  if(team == "Team B")  {

            if( run_index == 0) {
                last_ball_run_b.setText("W");
            } else {
                last_ball_run_b.setText(""+ run_array[run_index]+" runs");
            }

            team_run_b.setText("" + scoreTeamB + "/" + outTeamB);

            batsmenB[outTeamB].setScore(String.valueOf(batsmen_B[outTeamB]));
            overs_bowled_b.setText("" + oversTeamB + "." + num_balls_B);
            itemsAdapterB.notifyDataSetChanged();

            teamAButton.setEnabled(true);

        }

        if(batting_team_count == 2) {

            teamAButton.setEnabled(false);
            teamBButton.setEnabled(false);

            if(scoreTeamA > scoreTeamB){

                layoutTeamA.setBackgroundColor(getResources().getColor(android.R.color.holo_green_light ));
                layoutTeamB.setBackgroundColor(getResources().getColor(android.R.color.darker_gray ));

                current_batting_team.setText("Team - " + getResources().getText(R.string.team_a) + " wins the match");
            } else if(scoreTeamB > scoreTeamA) {

                layoutTeamB.setBackgroundColor(getResources().getColor(android.R.color.holo_green_light));
                layoutTeamA.setBackgroundColor(getResources().getColor(android.R.color.darker_gray ));

                current_batting_team.setText("Team - " + getResources().getText(R.string.team_b) + " wins the match");
            } else {
                layoutTeamB.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
                layoutTeamA.setBackgroundColor(getResources().getColor(android.R.color.darker_gray ));

                current_batting_team.setText("The match is tied");
            }

        }

    }

    /**
     * cumulative score of team B
     * @param view
     */
    public void scoreTeamB(View view) {

        int run_index = new Random().nextInt(MAX_THROWS_OVER);
        if( (num_balls_B+1) % MAX_THROWS_OVER == 0 ) {
            oversTeamB++;
            num_balls_B = 0;

        } else { num_balls_B++; }

        if(run_array[run_index] == -1) {
            batsmen_B[outTeamB] += 0;
            outTeamB++;
            if(outTeamB == MAX_OUT){
                batting_team_count++;
                teamBButton.setEnabled(false);
            }

        } else {
            scoreTeamB += run_array[run_index];
            batsmen_B[outTeamB] += run_array[run_index];
        }

        if(oversTeamB == MAX_OVERS) {  //
            batting_team_count++;
            teamBButton.setEnabled(false);
        }

        displayToastAndToggleButton(oversTeamB, outTeamB, teamBButton, getResources().getText(R.string.team_b).toString());

        if(outTeamB <= MAX_OUT) {
            displayScore(run_index,"Team B");
        }


    }

    /**
     * resets the view window to initial state
     * @param view
     */
    public void resetTeamScores(View view) {

        scoreTeamA = 0;  num_balls_A = 0;
        scoreTeamB = 0;  num_balls_B = 0;
        outTeamA = 0;    oversTeamA = 0;
        outTeamB = 0;    oversTeamB = 0;
        batting_team_count = 0;

        initBatsmen();

        teamAButton.setEnabled(true);
        teamBButton.setEnabled(true);

        initViews();

    }
}
