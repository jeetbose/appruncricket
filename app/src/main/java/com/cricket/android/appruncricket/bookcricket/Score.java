package com.cricket.android.appruncricket.bookcricket;

public class Score {

    private String score;
    private String name;


    public Score(String name, String score) {
        this.score = score;
        this.name = name;
    }

    public String getScore() {
        return score;
    }

    public String getName() {
        return name;
    }

    public void setScore(String  score) {
        this.score = score;
    }

}
